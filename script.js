//=============================================================================================

document.getElementById("countButton").onclick = function()
{
    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase(); //All letters to lowercase
    const words = typedText.split(" ");
    typedText = typedText.replace(/[^a-z'\s]+/g, "");


    const letterCounts = {};    //(key: #)
    const wordCounts = {};

    for(let i = 0; i < typedText.length; i++)
    {
        currentLetter = typedText[i];

        if(letterCounts[currentLetter] === undefined)   //letterCounts = {} associated with currentLetter
        {               //{ :H} == undefined so no value or number for letterCounts
            letterCounts[currentLetter] = 1;
        }
        else
        {
            letterCounts[currentLetter]++;      //if it's a letter give +1
        }
    }

    for (let letter in letterCounts) 
    {
        const newElement = document.createElement("div");
        newElement.style.backgroundColor = "lightgreen";
        newElement.style.width = 27 + "%";
        const newText = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
        newElement.appendChild(newText);
        document.getElementById("lettersDiv").appendChild(newElement);
    }
    
    for(let i = 0; i < words.length; i++)
    {
        currentLetter = words[i];

        if(wordCounts[currentLetter] === undefined)   //letterCounts = {} associated with currentLetter
        {               //{ :H} == undefined so no value or number for wordCounts
            wordCounts[currentLetter] = 1;
        }
        else
        {
            wordCounts[currentLetter]++;      //if it's a letter give +1
        }
    }
    for (let letter in wordCounts) 
    {
        const newElement = document.createElement("span");
        newElement.style.backgroundColor = "orange";
        const newText = document.createTextNode('"' + letter + "\": " + wordCounts[letter] + ", ");
        newElement.appendChild(newText);
        document.getElementById("wordsDiv").appendChild(newElement);
    }

}
